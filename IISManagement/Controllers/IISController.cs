﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace IISManagement.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class IISController : ControllerBase
	{
		private readonly ILogger<WeatherForecastController> _logger;
		private readonly HttpClient _apiClient;

		public IISController(ILogger<WeatherForecastController> logger)
		{
			_logger = logger;

			// HTTP client
			_apiClient = new HttpClient(new HttpClientHandler()
			{
				UseDefaultCredentials = true
			}, true); ;

			// Set access token for every request
			_apiClient.DefaultRequestHeaders.Add("Access-Token", "Bearer ynUtemuiEeGdsZvquYHnBFkthjoWeprA0qNIrv1K79qf664lGHOBUA");
			// Request HAL (_links)
			_apiClient.DefaultRequestHeaders.Add("Accept", "application/hal+json");
		}

		[HttpGet]
		public async Task<IActionResult> GetAsync()
		{
			// Get Websites
			var res = await _apiClient.GetAsync("https://localhost:55539/api/webserver/websites");

			if (res.StatusCode != HttpStatusCode.OK)
			{
				return BadRequest(res);
			}

			var response = res.Content.ReadAsStringAsync();

			return Ok(response);
		}

		[HttpPost]
		public async Task<IActionResult> CreateAsync([FromBody] Site site)
		{
			var physicalPath = @"C:\inetpub\wwwroot";
			var sitePath = $"{physicalPath}\\{site.Name}";
			
			// Create folder
			System.IO.Directory.CreateDirectory(sitePath);

			var newSite = new
			{
				name = site.Name,
				physical_path = sitePath,
				bindings = new object[] {
					new {
						port = site.Port,
						protocol = site.Protocol,
						ip_address = site.IpAddress,
						hostname = site.HostName
					}
				}
			};

			// Get Websites
			var res = await _apiClient.PostAsync("https://localhost:55539/api/webserver/websites",
				new StringContent(JsonConvert.SerializeObject(newSite), Encoding.UTF8, "application/json"));


			if (res.StatusCode != HttpStatusCode.Created)
			{
				return BadRequest(res);
			}

			var response = res.Content.ReadAsStringAsync();

			return Ok(response);
		}
	}

	public class Site
	{
		public string Name { get; set; }
		public string HostName { get; set; }
		public string Protocol { get; set; }
		public string IpAddress { get; set; }
		public int Port { get; set; }
	}
}
